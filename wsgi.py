#!/usr/bin/env python

"""#!flask/bin/python"""

from flask import Flask, jsonify, abort, make_response, request
from flask_restful import Api, Resource, reqparse, fields, marshal
from flask_httpauth import HTTPBasicAuth
from sqlalchemy import *
from json import dumps

db_connect = create_engine('sqlite:///threadit.db')
db_metadata = MetaData()
db_threads = Table('THREADS', db_metadata,
    Column('thread_id', Integer, primary_key=true),
    Column('thread_name', String),
    Column('thread_plus', Integer),
    Column('thread_minus', Integer),
    Column('thread_subthread', String),
    Column('thread_text', String),
)
db_threads = Table('THREADS',db_metadata)
db_metadata.create_all(db_connect)

application = Flask(__name__, static_url_path="")
api = Api(application)
auth = HTTPBasicAuth()

@auth.get_password
def get_password(username):
    if username == 'test':
        return 'test'
    return None


@auth.error_handler
def unauthorized():
    # return 403 instead of 401 to prevent browsers from displaying the default
    # auth dialog
    return make_response(jsonify({'message': 'Unauthorized access'}), 403)

threads_fields = {
    'thread_name': fields.String,
    'thread_plus': fields.Integer,
    'thread_minus': fields.Integer,
    'thread_subthread': fields.String,
    'thread_text': fields.String,
    'uri': fields.Url('thread')
}


"""Id, Name, Plus, Minus, Subthread"""
class ThreadListAPI(Resource):
    decorators = [auth.login_required]

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('thread_name', type=str, required=True,
                                   help='No thread name provided',
                                   location='json')
        self.reqparse.add_argument('thread_subthread', type=str, default="",
                                   location='json')
        self.reqparse.add_argument('thread_text', type=str, default="",
                                   location='json')
        super(ThreadListAPI, self).__init__()

    def get(self):
        conn = db_connect.connect()
        s = select([db_threads])
        result = conn.execute(s)

        row = 0
        if(row == None):
            abort(404)
        return {'threads': [marshal(thread, threads_fields) for thread in result ]}


    def post(self):
        args = self.reqparse.parse_args()
        ins = db_threads.insert().values(
            thread_name=args['thread_name'],
            thread_plus=0,
            thread_minus=0,
            thread_subthread=args['thread_subthread'],
            thread_text=args['thread_text']
            )
        conn = db_connect.connect()
        result = conn.execute(ins)
        thread = {
            'thread_id': result.lastrowid,
            'thread_name': args['thread_name'],
            'thread_subthread': args['thread_subthread'],
            'thread_plus': 0,
            'thread_minus': 0,
            'thread_text': args['thread_text'],
        }
        return {'thread': marshal(thread, threads_fields)}, 201

class ThreadAPI(Resource):
    decorators = [auth.login_required]
    
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('thread_name', type=str,
                                   location='json')
        self.reqparse.add_argument('thread_subthread', type=str,
                                   location='json')
        self.reqparse.add_argument('thread_plus', type=int,
                                   location='json')
        self.reqparse.add_argument('thread_minus', type=int,
                                   location='json')
        self.reqparse.add_argument('thread_text', type=str,
                                   location='json')
        super(ThreadAPI, self).__init__()

    def get(self,thread_id):
        conn = db_connect.connect()
        s = select([db_threads]).where(db_threads.c.thread_id == thread_id)
        result = conn.execute(s)
        row = result.fetchone()
        if row == None:
            abort(404) 
        thread = {
            'thread_id': row['thread_id'],
            'thread_name': row['thread_name'],
            'thread_subthread': row['thread_subthread'],
            'thread_plus': row['thread_plus'],
            'thread_minus': row['thread_minus'],
            'thread_text': row['thread_text'],
        }
        return {'thread': marshal(thread, threads_fields)}

    def put(self,thread_id):
        args = self.reqparse.parse_args()

        conn = db_connect.connect()
        trans = conn.begin()
        try:            
            for k, v in args.items():

                if v is not None:
                    if(k == "thread_name"):
                        update = db_threads.update().values(thread_name=v).where(
                            db_threads.c.thread_id == thread_id)
                    elif(k == "thread_subthread"):
                        update = db_threads.update().values(thread_subthread=v).where(
                            db_threads.c.thread_id == thread_id)
                    elif(k == "thread_text"):
                        update = db_threads.update().values(thread_text=v).where(
                            db_threads.c.thread_id == thread_id)
                    elif(k == "thread_plus"):
                        update = db_threads.update().values(thread_plus=v).where(
                            db_threads.c.thread_id == thread_id)
                    elif(k == "thread_minus"):
                        update = db_threads.update().values(thread_minus=v).where(
                            db_threads.c.thread_id == thread_id)
                    else:
                        break
                    conn.execute(update)
            trans.commit()
        except:
            trans.rollback()
            raise


        s = select([db_threads]).where(db_threads.c.thread_id == thread_id)

        result = conn.execute(s)
        row = result.fetchone()


        

        return {'thread': marshal(row, threads_fields)}

    def delete(self, thread_id):
        conn = db_connect.connect()
        s = select([db_threads]).where(db_threads.c.thread_id == thread_id)
        result = conn.execute(s)
        row = result.fetchone()

        if row == None:
            abort(404)
        conn = db_connect.connect()
        conn.execute(db_threads.delete().where(db_threads.c.thread_id == thread_id))
        return {'result': True}

api.add_resource(ThreadListAPI, '/threadit/api/v1.0/threads', endpoint='threads')
api.add_resource(ThreadAPI, '/threadit/api/v1.0/threads/<int:thread_id>', endpoint='thread')


if __name__ == '__main__':
    application.run(debug=True)