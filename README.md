Reddit like web app build on Python server with KnockoutJS client.

Used tutorials:
https://blog.miguelgrinberg.com/post/writing-a-javascript-rest-client
https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask

Python SQLite documentation:
http://docs.sqlalchemy.org/en/latest/

KnockoutJS documentation:
http://knockoutjs.com/documentation/introduction.html